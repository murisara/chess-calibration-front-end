from PyQt5 import QtWidgets, uic
from PyQt5.QtCore import QRunnable, QObject, pyqtSlot, pyqtSignal
from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.backends.backend_qt5agg import NavigationToolbar2QT as NavigationToolbar
from pyqtgraph.Qt import QtGui, QtCore
import pyqtgraph as pg
import threading
from pathlib import Path
import os
import sys
sys.path.insert(1,'/home/duncan/sara-scripts-and-notebooks/')
sys.path.insert(1,'CA_scripts/')
import numpy as np
import matplotlib
matplotlib.use('Qt5Agg')
import matplotlib.pyplot as plt
import time
from h5tolinescan_class import ZingerBGone, LineScan 
from threading import Thread
from multiprocessing import Process, Queue, Pool
from CondtoPath import condtopath, auxpath
import glob
import gzip


QtWidgets.QApplication.setAttribute(QtCore.Qt.AA_EnableHighDpiScaling, True)

class Ui(QtWidgets.QMainWindow):
    def __init__(self):
        super(Ui, self).__init__() # Call the inherited classes __init__ method
        uic.loadUi('CHESS_Align.ui', self) # Load the .ui file
        self.show() # Load the .ui file
        self.setWindowTitle('CHESS Align')
        self.WorkDir = os.getcwd()
        self.oldMatSysName = None
        self.currentSelection = None


        # all the functionality
        self.MatSysNameLineEdit = self.findChild(QtWidgets.QLineEdit,'MatSysNameLineEdit')
        self.MatSysNameLineEdit.editingFinished.connect(self.OnReturnMatSysName)
        self.MatSysNameLineEdit.setText('lol IDK')


        self.PadLocID = self.findChild(QtWidgets.QLabel,'PadLocID')
        self.ActiveAction = self.findChild(QtWidgets.QTextBrowser,'ActiveAction')
        self.xshiftSpinBox = self.findChild(QtWidgets.QDoubleSpinBox,'xshifSpinBox')
        self.yshiftSpinBox = self.findChild(QtWidgets.QDoubleSpinBox,'yshifSpinBox')
        
        self.DAQPathLineEdit = self.findChild(QtWidgets.QLineEdit,'DAQPathLineEdit')
        self.LoadDAQPathButton = self.findChild(QtWidgets.QPushButton,'LoadDAQPathButton')
        self.LoadDAQPathButton.clicked.connect(self.onClickedOpenFileNameDialog_DAQ)

        self.AUXPathLineEdit = self.findChild(QtWidgets.QLineEdit,'AUXPathLineEdit')
        self.LoadAUXPathButton = self.findChild(QtWidgets.QPushButton,'LoadAUXPathButton')
        self.LoadAUXPathButton.clicked.connect(self.onClickedOpenFileNameDialog_AUX)

        self.CollectPadsButton = self.findChild(QtWidgets.QPushButton,'CollectPadsButton')
        self.CollectPadsButton.clicked.connect(self.OnClickedStartAlign)

        self.WaferLoadedCheckBox = self.findChild(QtWidgets.QCheckBox,'WaferLoadedCheckBox')
        self.WaferLoadedCheckBox.clicked.connect(self.onClickCheckChecklist)
        self.VacuumOnCheckBox = self.findChild(QtWidgets.QCheckBox,'VacuumOnCheckBox')
        self.VacuumOnCheckBox.clicked.connect(self.onClickCheckChecklist)
        self.OpticallyAlignedCheckBox = self.findChild(QtWidgets.QCheckBox,'OpticallyAlignedCheckBox')
        self.OpticallyAlignedCheckBox.clicked.connect(self.onClickCheckChecklist)
        self.HutchClosedCheckBox = self.findChild(QtWidgets.QCheckBox,'HutchClosedCheckBox')
        self.HutchClosedCheckBox.clicked.connect(self.onClickCheckChecklist)
        self.SpecConnectedCheckBox = self.findChild(QtWidgets.QCheckBox,'SpecConnectedCheckBox')
        self.SpecConnectedCheckBox.clicked.connect(self.onClickCheckChecklist)
        self.DataPathsSpecifiedCeckBox = self.findChild(QtWidgets.QCheckBox,'DataPathsSpecifiedCheckBox')
        self.DataPathsSpecifiedCeckBox.clicked.connect(self.onClickCheckChecklist)
        self.MatSysNameSpecifiedCheckBox = self.findChild(QtWidgets.QCheckBox,'MatSysNameSpecifiedCheckBox')
        self.MatSysNameSpecifiedCheckBox.clicked.connect(self.onClickCheckChecklist)
        self.checklist = [
            self.WaferLoadedCheckBox,
            self.VacuumOnCheckBox,
            self.OpticallyAlignedCheckBox,
            self.HutchClosedCheckBox,
            self.SpecConnectedCheckBox,
            self.DataPathsSpecifiedCeckBox,
            self.MatSysNameSpecifiedCheckBox
        ]



        #PlotSetup
        self.clickedPen = pg.mkPen('b', width=2)
        self.lastClicked = []

        self.LineScanProfilePlot = self.findChild(QtWidgets.QGraphicsView,'LineScanProfilePlot')
        self.LineScanPlot = self.LineScanProfilePlot.plot()
        # self.LineScanProfilePlot.setBackground('w')
        self.LineScanProfilePlot.setTitle('Linescan Plot')
        self.LineScanProfilePlot.setLabel('left', 'Intensity', units='a.u.')
        self.LineScanProfilePlot.setLabel('bottom', 'Scan length', units='mm')
        self.LineScanProfilePlot.setXRange(-3, 3)
        self.LineScanProfilePlot.setYRange(0, 1)


        # self.testWM_data = np.genfromtxt('TestWaferMap.csv',delimiter=',',skip_header=1)

        self.WaferPosAndOffsetPlot = self.findChild(QtWidgets.QGraphicsView,'WaferPosAndOffestPlot')
        waferEdge = QtGui.QGraphicsEllipseItem(-50,-50,100,100)
        waferEdge.setBrush(pg.mkBrush(8, 3, 132))
        self.WaferPosAndOffsetPlot.addItem(waferEdge)
        self.WaferPosAndOffsetPlot.setAspectLocked(True, ratio=1)
        self.WaferPlot = pg.ScatterPlotItem(pen=pg.mkPen('w'), pxMode=True, hoverable=True, hoverPen=pg.mkPen(270, 67, 48))
        spots = []
        self.AlignPositions = np.array([[+00., +00.],
                            [+00., +40.],
                            [+32., +30.],
                            [+40., +00.],
                            [+32., -30.],
                            [+00., -40.],
                            [-32., -30.],
                            [-40., +00.],
                            [-32., +30.]])

        for x,y in self.AlignPositions:
            rect = QtGui.QPainterPath()
            rect.addRect(-0.45,-2.25, 0.25, 7)
            spots.append({'pos':(x,y),'data':1,'brush':pg.mkBrush(255, 202, 58),'size':20 ,'symbol':rect})

        self.WaferPosAndOffsetPlot.setTitle('Wafer Map')
        self.WaferPosAndOffsetPlot.setLabel('left', 'y position', units='mm')
        self.WaferPosAndOffsetPlot.setLabel('bottom', 'x position', units='mm')
        self.WaferPosAndOffsetPlot.setXRange(-50, 50)
        self.WaferPosAndOffsetPlot.setYRange(-50, 50)
        # print(spots)
        self.WaferPlot.addPoints(spots)
        self.WaferPosAndOffsetPlot.addItem(self.WaferPlot)
        self.WaferPlot.sigClicked.connect(self.plotClicked)
        
    def updateLSPP(self):
        sorted(files, key=lambda t: os.stat(t).st_mtime)

    def plotClicked(self,plot, points):    
        for p in self.lastClicked:
            p.resetPen()
        print("clicked points", points)
        for p in points:
            p.setPen(self.clickedPen)
        self.lastClicked = points
        print(self.lastClicked.item)
        
        

    def OnClickedStartAlign(self):
        # #workflow starts.
        # - starts lasgo client
        # - starts spec connection
        # - writes the directories in the DAQ and AUX
        # - fires up the integrater
        # - needs to parallelize intensity "integration"
        # - interpolate y-shift map, write .txt files to the AUX directory
        self.calib = PadCollectionWf(
            daqpath = self.DAQPathLineEdit.text(),
            auxpath = self.AUXPathLineEdit.text(),
            MatSysName = self.MatSysNameLineEdit.text()
            )
        self.calib.collectPads_and_integrate()
        self.ActiveAction.append(f'{self.get_time()}:\nY-offsets written to {self.AUXPathLineEdit.text()}')
        for box in self.checklist:
            box.setChecked(False)
        self.CollectPadsButton.setEnabled(False)
        self.oldMatSysName = self.MatSysNameLineEdit.text()



    def OnReturnMatSysName(self): 
        self.ActiveAction.append(f'{self.get_time()}:\nnew Material System {self.MatSysNameLineEdit.text()}')
        if self.MatSysNameLineEdit.text() == self.oldMatSysName:
            self.CollectPadsButton.setEnabled(False)
            self.ActiveAction.append('Please fill in new Mat Sys Name')
        self.onClickCheckChecklist()


    def onClickedOpenFileNameDialog_DAQ(self):
        """
        Opens a file selection dialog
        """
        options = QtWidgets.QFileDialog.Options()
        options = QtWidgets.QFileDialog.DontUseNativeDialog
        dirName = QtWidgets.QFileDialog.getExistingDirectory()
        self.DAQPathLineEdit.setText(dirName)
        self.ActiveAction.append(f'{self.get_time()}:\nnew DAQ path {dirName}')

    def onClickedOpenFileNameDialog_AUX(self):
        """
        Opens a file selection dialog
        """
        options = QtWidgets.QFileDialog.Options()
        options = QtWidgets.QFileDialog.DontUseNativeDialog
        dirName = QtWidgets.QFileDialog.getExistingDirectory()
        self.AUXPathLineEdit.setText(dirName)
        self.ActiveAction.append(f'{self.get_time()}:\nnew auxiliary path {dirName}')

    def onClickCheckChecklist(self):
        """
        perform checklist check. enable process button when ready
        """
        checklist_check = []
        for box in self.checklist:
            checklist_check.append(box.isChecked())

        if (not all(checklist_check)) or (self.MatSysNameLineEdit.text() == self.oldMatSysName):
            self.ActiveAction.append('check yourself before you wreck yourself')
            self.CollectPadsButton.setEnabled(False)
        else:
            self.ActiveAction.append(f'{self.get_time()}:\nrun enabled')
            self.CollectPadsButton.setEnabled(True)


    def get_time(self):
        return time.asctime(time.localtime(time.time()))
      



class PadCollectionWf():
    def __init__(self,daqpath=None, auxpath=None, MatSysName=None, yshifts=None):
        self.daq = daqpath
        self.aux = auxpath
        self.MatSysName = MatSysName
        self.yshifts = yshifts

        self.wd = self.daq+self.MatSysName+'/'
        self.ad = self.aux+self.MatSysName+'/'

        # Alignment pad locations: 18 scans in total. 2 at each alignment pad one horizontal 'h-calib' and one vertical 'v-calib'
        self.AlignmentPadLocs  = np.array([
            #The vertical scans
            [+00., +00.],
            [+00., +40.],
            [+32., +30.],
            [+40., +00.],
            [+32., -30.],
            [+00., -40.],
            [-32., -30.],
            [-40., +00.],
            [-32., +30.],

            #The horizontal scans
            [+00., +00.],
            [+00., +40.],
            [+32., +30.],
            [+40., +00.],
            [+32., -30.],
            [+00., -40.],
            [-32., -30.],
            [-40., +00.],
            [-32., +30.],
        ])

        #Setup the SARA client info
        self.status = CurrentState()
        self.status.directory = self.wd

        #test the new yshift correction:
        if self.yshifts:
            self.points, self.values = CalibReader_2021(self.yshifts)

        # # Create the queue and threader
        # self.q = Queue()

        # # how many threads are we going to allow for
        # for x in range(1):
        #      #t = Thread(target=threader)
        #      self.t = Process(target=threader, args=[q])

        #      # classifying as a daemon, so they will die when the main dies
        #      self.t.daemon = True

        #      # begins, must come after daemon definition
        #      self.t.start()
    

    def connectToHardware(self):
        """
        Sets up the sockets for hardware and spec communication
        """
        self.address = "CHESS"
        self.socket_list = ["lasgo"] 
        self.socket_clients = sc.socket_clients()
        self.clients = self.socket_clients.get_clients(address, socket_list = socket_list)

        # Instantiate the laser connection
        self.laser = LSA_Laser.laser()
        self.laser.socket_clients = self.socket_clients

        #connect to the Spec Server
        self.psa = PySpecAgent()
        self.psa.ConnectToServer()

    def collectPads_and_integrate(self):
        """
        Alignment Pad data collection sequence
        """
        #iterate through each pad location
        LS = LineScan(daqpath=self.daq,auxpath=self.aux,MatSysName=self.MatSysName)
        LS.createOutPaths()
        for idx, padloc in enumerate(self.AlignmentPadLocs):
            #pass position info to SARA status 
            # print('cycle start')
            print(LS.yshift)
            self.status.pos = (padloc[0],padloc[1])
            if self.yshifts:
                self.yoffset = interpolate2d(points,values,self.status.pos,method='linear',scaling=1.2)
            else:
                self.yoffset=0
            # print('Status Pos')
            # print(status.pos)
           
            _,calibpath = condtopath(self.status)
            # print('Calibration daq read/write path')
            # print(calibpath)
            _,auxcalib = auxpath(self.status,auxprefix=self.ad)
            # print('Calibration aux read/write path')
            # print(auxcalib)
            #iterate through the horizontal and vertical scans at each location and set the SARA status, flyscan coords, naming convention, and integration params
            if idx<9:
            #Horizontal alignment scans 2 mm long
                padname = f'h-calib_pad_{int(padloc[0])}_{int(padloc[1])}'
                start = (self.status.pos[0] - 1.0 - 0.55, self.status.pos[1] + 1.3 + self.yoffset) #hscan start for the pad is -0.55 mm from pad origin and 1 mm before that 
                end =   (self.status.pos[0] + 1.0 - 0.55, self.status.pos[1] + 1.3 + self.yoffset) #hscan end for the pad is -0.55 mm from pad origin and 1 mm past that 
                # print(padname)
                # print(start,end)
            #Set flyscan params
                det_int_time = 50 #in milliseconds
                nframes = 201 #int frames

            else:
            # Vertical alignment scans 7 mm long
                padname = f'v-calib_pad_{int(padloc[0])}_{int(padloc[1])}'
                start = (self.status.pos[0] - 0.55, self.status.pos[1] - 3.5 + 1.3 + self.yoffset) #vscan start for the pad is 1.3 mm from pad origin and 2.5 mm before that 
                end =   (self.status.pos[0] - 0.55, self.status.pos[1] + 3.5 + 1.3 + self.yoffset) #vscan end for the pad is 1.3 mm from pad origin and 2.5 mm past that 
                # print(padname)
                # print(start,end)
            #Set flyscan params
                det_int_time = 50 #in milliseconds
                nframes = 351 #int frames
                # print(det_int_time,nframes)


            # simulate the data collection time
            # time.sleep(2)
                


            # #Synchrotron checks
            # #Check for time to fill. if there is < current det_int_time*nframes + 10s (buffer), wait the durration of the fill + 15s
            # ttf = self.psa.GetTimeToFill()
            # iclow = self.psa.GetIntensity()

            # # print(f'Time to fill: {ttf} seconds')
            # while (ttf < det_int_time/1000*nframes+3) or (iclow < 1.6):
            #     print(f'holding')
            #     time.sleep(1)
            #     ttf = self.psa.GetTimeToFill()
            #     iclow = self.psa.GetIntensity()




            # #Execution loop
            # self.psa.SetFilePaths(calibpath,padname)

            # #Spawn thread and look to the registered output for the "Armed" state
            # print("Is it armed?", "Armed" in self.psa.Spec.reg_channels['output/tty'].read())
            # time.sleep(4)
            # process = Thread(target=self.psa.SetFlyScan, args=[nframes, det_int_time/1000]) #spec takes an input of seconds for integration time, Mike likes milliseconds
            # process.start()
            # while "Armed" not in self.psa.Spec.reg_channels['output/tty'].read():
            #     print("Calling status", self.psa.Spec.reg_channels['output/tty'].read())
            #     print("Not ready yet")


            # #Code that executes the flyscan from lasgo
            # self.laser.execute_flyscan(start, end, det_int_time, nframes)
            # print("Calling status", self.psa.Spec.reg_channels['status/ready'].read())
            # print("Waiting for my thread to finish")
            # process.join()
            # print("The PySpec thread has finished")


            # #new code to check if files are written to DAQ in the appropriate folder:
            # detstate = self.psa.Get_WO_status()
            # while "Idle" not in detstate:
            #     detstate = self.psa.Get_WO_status()
            #     time.sleep(0.5)
            #     pass

            #simulated data collection = 0.05 seconds * nframes = on average 14 seconds read time of all the data is really slow

            #Rapid integerate/correlate (all functionality of h5 to linescan)
            inputs = sorted(glob.glob(os.path.join(self.daq,f'Calibration/PONIs/{padname}*/*')))[:-1]
            print(len(inputs))
            p = Process(target=LS.lsWorkflow,args=(inputs,padname))
            p.start()
            p.join()


def CalibReader_2021(path):
    """Extracts the (x,y) and z data from a .txt file"""
    raw = np.genfromtxt(path,delimiter=',',dtype=float)
    x = raw[:,0]
    y = raw[:,1]
    z = raw[:,2]
    points = []
    for xx, yy in zip(x, y):
        points.append([xx, yy])
    return points, z

class CurrentState():
    """
    Contains the current status of the stage.
    Used to pass parameters between the agents.
    """
    def __init__(self):
        self.image_error = True
        self.pos = [6., 25.]
        self.cond = [0., 0.]
        self.detector_info = {}
        self.directory = "/"

if __name__ == "__main__":
    app = QtWidgets.QApplication(sys.argv) # Create an instance of QtWidgets.QApplication
    window = Ui() # Create an instance of our class
    window.show()
    sys.exit(app.exec())
